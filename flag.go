package scythe

import (
	"fmt"

	"gopkg.in/yaml.v3"
)

type internalFlag struct {
	Name      string                  `yaml:"name"`
	Default   yaml.Node               `yaml:"default"`
	Overrides []*internalFlagOverride `yaml:"overrides"`
	Enabled   bool                    `yaml:"enabled"`
}

type internalFlagOverride struct {
	Value yaml.Node `yaml:"value"`
	Tests []string  `yaml:"tests"`
}

type flag struct {
	Name      string          `yaml:"name"`
	Default   yaml.Node       `yaml:"default"`
	Overrides []*flagOverride `yaml:"overrides"`
	Enabled   bool
}

type flagOverride struct {
	Value yaml.Node   `yaml:"value"`
	Tests []*flagTest `yaml:"tests"`
}

type flagTest struct {
	Attribute string
	Operator  string
	Values    []string
}

func (f *flag) getOverride(constraints map[string]any) (*flagOverride, error) {
	for _, override := range f.Overrides {
		passesTests, err := f.passesAllTests(override, constraints)
		if err != nil {
			return nil, err
		}

		if passesTests {
			return override, nil
		}
	}

	return nil, nil
}

func (f *flag) passesAllTests(override *flagOverride, constraints map[string]any) (bool, error) {
	for _, test := range override.Tests {
		passesTest, err := f.evaluateOperator(test, constraints)
		if err != nil {
			return false, err
		} else if !passesTest {
			return false, nil
		}
	}

	return true, nil
}

func (f *flag) evaluateOperator(test *flagTest, constraints map[string]any) (bool, error) {
	evaluator, ok := operators[test.Operator]
	if !ok {
		return false, fmt.Errorf("operator not supported: %s", test.Operator)
	}

	if _, ok := constraints[test.Attribute]; !ok {
		return false, fmt.Errorf("unknown constraint attribute: %s", test.Attribute)
	}

	return evaluator(f, test, constraints[test.Attribute])
}
