package scythe_test

import (
	"bytes"
	"testing"

	"github.com/mijara/scythe"
	"github.com/stretchr/testify/require"
)

func TestInOperator(t *testing.T) {
	reader := bytes.NewBufferString(`---
flag:
  default: false
  enabled: true
  overrides:
    - value: true
      tests:
        - string IN [a, b, c]
        - int IN [1, 2, 3]
        - float IN [1.5, 2.5, 3.5]`)

	client, err := scythe.New(reader)
	require.NoError(t, err)

	value, err := scythe.Eval(client, "flag", map[string]any{
		"string": "b",
		"int":    2,
		"float":  2.5,
	}, false)
	require.NoError(t, err)
	require.Equal(t, true, value)
}

func TestExpOperator(t *testing.T) {
	reader := bytes.NewBufferString(`---
flag:
  default: false
  enabled: true
  overrides:
    - value: true
      tests:
        - id EXP [0, 50]`)

	client, err := scythe.New(reader)
	require.NoError(t, err)

	tests := []struct {
		id   string
		want bool
	}{
		{
			id:   "3e94349c-7d74-4ee1-bc9c-05a679814068",
			want: true,
		},
		{
			id:   "6177cc2e-1b0e-419c-99ef-4c3e92bf02e6",
			want: false,
		},
	}

	for _, test := range tests {
		t.Run(test.id, func(t *testing.T) {
			value, err := scythe.Eval(client, "flag", map[string]any{
				"id": test.id,
			}, false)
			require.NoError(t, err)
			require.Equal(t, test.want, value)
		})
	}
}

func TestContainsOperator(t *testing.T) {
	reader := bytes.NewBufferString(`---
flag:
  default: false
  enabled: true
  overrides:
    - value: true
      tests:
        - strings CONTAINS a
        - ints CONTAINS 1
        - floats CONTAINS 1.5`)

	client, err := scythe.New(reader)
	require.NoError(t, err)

	value, err := scythe.Eval(client, "flag", map[string]any{
		"strings": []string{"a", "b", "c"},
		"ints":    []int{1, 2, 3},
		"floats":  []float64{1.5, 2.5, 3.5},
	}, false)
	require.NoError(t, err)
	require.Equal(t, true, value)
}

func TestEnabledAttribute(t *testing.T) {
	reader := bytes.NewBufferString(`---
flagEnabled:
  default: true
  enabled: true

flagDisabled:
  default: true
  enabled: false`)

	client, err := scythe.New(reader)
	require.NoError(t, err)

	tests := []struct {
		name string
		flag string
		err  bool
		want bool
	}{
		{
			name: "enabled true flag",
			flag: "flagEnabled",
			want: true,
		},
		{
			name: "disabled true flag",
			flag: "flagDisabled",
			err:  true,
			want: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			value, err := scythe.Eval(client, test.flag, map[string]any{}, false)

			if !test.err {
				require.NoError(t, err)
			} else {
				require.Error(t, err)
			}

			require.Equal(t, test.want, value)
		})
	}
}
