package scythe

import "fmt"

// Eval evaluates a flag against the constraints passed.
// The flag default will be returned only in case the constraints don't match any
// flag override, otherwise any configuration error will always render defaultValue instead,
// along with an error message.
func Eval[T any](client *Client, key string, constraints map[string]any, defaultValue T) (T, error) {
	preparedConstraints := client.prepareConstraints(constraints)

	if err := client.validate(preparedConstraints); err != nil {
		return defaultValue, err
	}

	theFlag, err := client.getFlag(key)
	if err != nil {
		return defaultValue, err
	}

	if !theFlag.Enabled {
		return defaultValue, fmt.Errorf("reading disabled flag: %s", theFlag.Name)
	}

	override, err := theFlag.getOverride(preparedConstraints)
	if err != nil {
		return defaultValue, err
	}

	// if any override is found, try to use it's value, otherwise if none
	// is found use the flag default.
	value := theFlag.Default
	if override != nil {
		value = override.Value
	}

	obj := new(T)
	if err := value.Decode(obj); err != nil {
		return defaultValue, err
	}

	return *obj, nil
}
