package scythe

import (
	"fmt"
	"io"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Client struct {
	config *clientConfig
	flags  map[string]*flag
}

type clientConfig struct {
	strictAttributes   map[string]struct{}
	defaultConstraints map[string]any
}

func New(reader io.Reader, options ...clientOption) (*Client, error) {
	flags, err := loadFlags(reader)
	if err != nil {
		return nil, err
	}

	config := &clientConfig{}
	for _, opt := range options {
		opt(config)
	}

	return &Client{
		config: config,
		flags:  flags,
	}, nil
}

// Watch blocks execution until the next change in the flags file.
// Wrap this call into a for loop to keep watching forever.
func (c *Client) Watch(file string) error {
	if err := watchFile(file); err != nil {
		return err
	}

	fp, err := os.Open("flags.yaml")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	flags, err := loadFlags(fp)
	if err != nil {
		return err
	}

	c.flags = flags

	return nil
}

func (c *Client) getFlag(key string) (*flag, error) {
	theFlag, ok := c.flags[key]
	if !ok {
		return nil, fmt.Errorf("flag does not exist: %s", key)
	}
	return theFlag, nil
}

func (c *Client) validate(constraints map[string]any) error {
	if len(c.config.strictAttributes) > 0 {
		for key := range constraints {
			if _, ok := c.config.strictAttributes[key]; !ok {
				return fmt.Errorf("invalid attribute used in constraints: %s", key)
			}
		}
	}

	return nil
}

func (c *Client) prepareConstraints(constraints map[string]any) map[string]any {
	if c.config.defaultConstraints == nil {
		return constraints
	}

	result := make(map[string]any)

	for k, v := range c.config.defaultConstraints {
		result[k] = v
	}

	for k, v := range constraints {
		result[k] = v
	}

	return result
}

func loadFlags(reader io.Reader) (map[string]*flag, error) {
	internalFlags := make(map[string]*internalFlag)
	if err := yaml.NewDecoder(reader).Decode(&internalFlags); err != nil {
		return nil, err
	}

	flags := make(map[string]*flag)
	for name, theInternalFlag := range internalFlags {
		theInternalFlag.Name = name
		theFlag, err := mapInternalFlagToFlag(theInternalFlag)
		if err != nil {
			return nil, err
		}

		flags[name] = theFlag
	}

	return flags, nil
}

func mapInternalFlagToFlag(theFlag *internalFlag) (*flag, error) {
	overrides := make([]*flagOverride, 0, len(theFlag.Overrides))
	for _, override := range theFlag.Overrides {
		tests := make([]*flagTest, 0, len(override.Tests))
		for _, test := range override.Tests {
			test, err := parseTest(test)
			if err != nil {
				return nil, err
			}

			tests = append(tests, test)
		}

		override := &flagOverride{
			Value: override.Value,
			Tests: tests,
		}

		overrides = append(overrides, override)
	}

	return &flag{
		Name:      theFlag.Name,
		Default:   theFlag.Default,
		Overrides: overrides,
		Enabled:   theFlag.Enabled,
	}, nil
}

type clientOption func(cfg *clientConfig)

// WithStrictAttributes sets the set of allowed attributes that can be used in constraints.
func WithStrictAttributes(attributes ...string) clientOption {
	return func(cfg *clientConfig) {
		set := make(map[string]struct{})
		for _, attr := range attributes {
			set[attr] = struct{}{}
		}

		cfg.strictAttributes = set
	}
}

// WithDefaultConstraints sets constraints that will be used for any evaluation,
// Eval() constraints are applied on top of default constraints.
func WithDefaultConstraints(constraints map[string]any) clientOption {
	return func(cfg *clientConfig) {
		cfg.defaultConstraints = constraints
	}
}
