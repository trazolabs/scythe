package scythe

import (
	"errors"
	"fmt"
	"slices"
	"strconv"
)

var operators = map[string]operator{
	"CONTAINS": containsOperator,
	"IN":       inOperator,
	"EXP":      expOperator,
	"BETWEEN":  betweenOperator,
	"==":       makeComparison(&equalComparator{}),
	"!=":       makeComparison(&notEqualComparator{}),
	">":        makeComparison(&greaterComparator{}),
	">=":       makeComparison(&greaterEqualComparator{}),
	"<":        makeComparison(&lessComparator{}),
	"<=":       makeComparison(&lessEqualComparator{}),
}

type operator func(f *flag, condition *flagTest, constraintValue any) (bool, error)

func makeComparison(cmp comparator) operator {
	return func(f *flag, condition *flagTest, constraintValue any) (bool, error) {
		switch typedConstraintValue := constraintValue.(type) {
		case string:
			return cmp.String(typedConstraintValue, condition.Values[0]), nil
		case int:
			flagValueInt, err := strconv.ParseInt(condition.Values[0], 10, 64)
			if err != nil {
				return false, err
			}
			return cmp.Int(typedConstraintValue, int(flagValueInt)), nil
		case float64:
			flagValueFloat, err := strconv.ParseFloat(condition.Values[0], 64)
			if err != nil {
				return false, err
			}
			return cmp.Float(typedConstraintValue, flagValueFloat), nil
		default:
			return false, fmt.Errorf("unsupported constraint type: %T", constraintValue)
		}
	}
}

type comparator interface {
	String(a, b string) bool
	Int(a, b int) bool
	Float(a, b float64) bool
}

type equalComparator struct{}

func (c *equalComparator) String(a, b string) bool { return a == b }
func (c *equalComparator) Int(a, b int) bool       { return a == b }
func (c *equalComparator) Float(a, b float64) bool { return a == b }

type notEqualComparator struct{}

func (c *notEqualComparator) String(a, b string) bool { return a != b }
func (c *notEqualComparator) Int(a, b int) bool       { return a != b }
func (c *notEqualComparator) Float(a, b float64) bool { return a != b }

type greaterComparator struct{}

func (c *greaterComparator) String(a, b string) bool { return a > b }
func (c *greaterComparator) Int(a, b int) bool       { return a > b }
func (c *greaterComparator) Float(a, b float64) bool { return a > b }

type greaterEqualComparator struct{}

func (c *greaterEqualComparator) String(a, b string) bool { return a >= b }
func (c *greaterEqualComparator) Int(a, b int) bool       { return a >= b }
func (c *greaterEqualComparator) Float(a, b float64) bool { return a >= b }

type lessComparator struct{}

func (c *lessComparator) String(a, b string) bool { return a < b }
func (c *lessComparator) Int(a, b int) bool       { return a < b }
func (c *lessComparator) Float(a, b float64) bool { return a < b }

type lessEqualComparator struct{}

func (c *lessEqualComparator) String(a, b string) bool { return a <= b }
func (c *lessEqualComparator) Int(a, b int) bool       { return a <= b }
func (c *lessEqualComparator) Float(a, b float64) bool { return a <= b }

func containsOperator(f *flag, condition *flagTest, constraintValue any) (bool, error) {
	switch constraintValueAsType := constraintValue.(type) {
	case []string:
		return slices.Contains(constraintValueAsType, condition.Values[0]), nil
	case []int:
		flagValueInt, err := strconv.ParseInt(condition.Values[0], 10, 64)
		if err != nil {
			return false, err
		}
		return slices.Contains(constraintValueAsType, int(flagValueInt)), nil
	case []float64:
		flagValueFloat, err := strconv.ParseFloat(condition.Values[0], 64)
		if err != nil {
			return false, err
		}
		return slices.Contains(constraintValueAsType, flagValueFloat), nil
	}

	return false, fmt.Errorf("unsupported constraint type: %T", constraintValue)
}

func inOperator(f *flag, condition *flagTest, constraintValue any) (bool, error) {
	switch constraintValueAsType := constraintValue.(type) {
	case string:
		return slices.Contains(condition.Values, constraintValueAsType), nil
	case int:
		flagSlice, err := parseInts(condition.Values)
		if err != nil {
			return false, err
		}
		return slices.Contains(flagSlice, constraintValueAsType), nil
	case float64:
		flagSlice, err := parseFloats(condition.Values)
		if err != nil {
			return false, err
		}
		return slices.Contains(flagSlice, constraintValueAsType), nil
	}

	return false, fmt.Errorf("unsupported constraint type: %T", constraintValue)
}

func expOperator(f *flag, condition *flagTest, constraintValue any) (bool, error) {
	expRange, err := parseInts(condition.Values)
	if err != nil {
		return false, err
	}

	a, b, err := validateRange(expRange)
	if err != nil {
		return false, err
	}

	if a < 0 || b > 100 {
		return false, errors.New("range must be limited from 0 to 100")
	}

	bucket := getRolloutBucketWithSeed(constraintValue, f.Name)
	return bucket >= a && bucket < b, nil
}

func betweenOperator(f *flag, condition *flagTest, constraintValue any) (bool, error) {
	expRange, err := parseInts(condition.Values)
	if err != nil {
		return false, err
	}

	constraintAsInt, ok := constraintValue.(int)
	if !ok {
		return false, errors.New("between operator only supports int constraints")
	}

	a, b, err := validateRange(expRange)
	if err != nil {
		return false, err
	}

	return constraintAsInt >= a && constraintAsInt < b, nil
}
