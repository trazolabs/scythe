package scythe

import (
	"fmt"
	"slices"
	"strings"
	"unicode"
)

var allowedOperatorKeywords = []string{"CONTAINS", "IN", "EXP", "BETWEEN"}

func parseTest(text string) (*flagTest, error) {
	return newParser(text).parse()
}

type parser struct {
	text    []rune
	pointer int
}

func newParser(text string) *parser {
	return &parser{
		text:    []rune(text),
		pointer: 0,
	}
}

func (p *parser) parse() (*flagTest, error) {
	attribute, err := p.readKeyword()
	if err != nil {
		return nil, err
	}

	p.skipWhitespace()
	operator, err := p.readOperator()
	if err != nil {
		return nil, err
	}

	p.skipWhitespace()
	values, err := p.readRest()
	if err != nil {
		return nil, err
	}

	return &flagTest{
		Attribute: attribute,
		Values:    values,
		Operator:  operator,
	}, nil
}

func (p *parser) readRest() ([]string, error) {
	if p.pointer >= len(p.text) {
		return []string{""}, nil
	}

	if p.peek() == '[' {
		values, err := p.readValues()
		if err != nil {
			return nil, err
		}

		if p.pointer < len(p.text) {
			return nil, fmt.Errorf("invalid list definition: %s", string(p.text))
		}

		return values, nil
	}

	return []string{string(p.text[p.pointer:])}, nil
}

func (p *parser) readOperator() (string, error) {
	switch p.peek() {
	case '=':
		p.next()
		if p.peek() == '=' {
			p.next()
			return "==", nil
		}

		return "", fmt.Errorf("unexpected character :%c", p.peek())
	case '!':
		p.next()
		if p.peek() == '=' {
			p.next()
			return "!=", nil
		}

		return "", fmt.Errorf("unexpected character :%c", p.peek())
	case '>':
		p.next()
		if p.peek() == '=' {
			p.next()
			return ">=", nil
		}

		return ">", nil
	case '<':
		p.next()
		if p.peek() == '=' {
			p.next()
			return "<=", nil
		}

		return "<", nil
	case 'E', 'I', 'C', 'B':
		keyword, err := p.readKeyword()
		if err != nil {
			return "", err
		}

		if !slices.Contains(allowedOperatorKeywords, keyword) {
			return "", fmt.Errorf("expected one of %v, got: %s", allowedOperatorKeywords, keyword)
		}

		return keyword, nil
	}

	return "", fmt.Errorf("unexpected character: %c", p.peek())
}

func (p *parser) skipWhitespace() {
	for p.pointer < len(p.text) {
		if !unicode.IsSpace(p.peek()) {
			break
		}
		p.next()
	}
}

func (p *parser) readKeyword() (string, error) {
	var result strings.Builder

	for {
		current := p.peek()

		if unicode.IsLetter(current) || unicode.IsDigit(current) || current == '_' {
			result.WriteRune(current)
			p.next()
		} else {
			return result.String(), nil
		}
	}
}

func (p *parser) readValues() ([]string, error) {
	values := make([]string, 0)
	var currentValue strings.Builder

	for p.pointer < len(p.text) {
		current := p.peek()
		p.next()

		if current == '[' {
			continue
		} else if current == ']' {
			if currentValue.Len() > 0 {
				values = append(values, currentValue.String())
			}
			return values, nil
		} else if current == ',' {
			values = append(values, currentValue.String())
			currentValue.Reset()
		} else if !unicode.IsSpace(current) {
			currentValue.WriteRune(current)
		}
	}

	return nil, fmt.Errorf("invalid list definition: %s", string(p.text))
}

func (p *parser) peek() rune {
	return p.text[p.pointer]
}

func (p *parser) next() {
	p.pointer += 1
}
