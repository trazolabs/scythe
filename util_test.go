package scythe

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseInts(t *testing.T) {
	tests := []struct {
		name string
		give []string
		want []int
		err  bool
	}{
		{
			name: "success",
			give: []string{"1", "2", "3", "4"},
			want: []int{1, 2, 3, 4},
			err:  false,
		},
		{
			name: "failure",
			give: []string{"1", "a", "3", "4"},
			err:  true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := parseInts(test.give)
			if test.err {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, test.want, result)
			}
		})
	}
}

func TestParseFloats(t *testing.T) {
	tests := []struct {
		name string
		give []string
		want []float64
		err  bool
	}{
		{
			name: "success",
			give: []string{"1.5", "2.5", "3.5", "4.5"},
			want: []float64{1.5, 2.5, 3.5, 4.5},
			err:  false,
		},
		{
			name: "failure",
			give: []string{"1.5", "a", "3.5", "4.5"},
			err:  true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := parseFloats(test.give)
			if test.err {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, test.want, result)
			}
		})
	}
}
