package scythe

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/spaolacci/murmur3"
)

func transformSlice[T any](slice []string, transformFunc func(v string) (T, error)) ([]T, error) {
	values := make([]T, 0, len(slice))
	for _, valueString := range slice {
		value, err := transformFunc(valueString)
		if err != nil {
			return nil, err
		}

		values = append(values, value)
	}

	return values, nil
}

func parseInts(values []string) ([]int, error) {
	return transformSlice(values, func(v string) (int, error) {
		value, err := strconv.ParseInt(v, 10, 64)
		return int(value), err
	})
}

func parseFloats(values []string) ([]float64, error) {
	return transformSlice(values, func(v string) (float64, error) {
		return strconv.ParseFloat(v, 64)
	})
}

func validateRange(rng []int) (int, int, error) {
	if len(rng) != 2 {
		return 0, 0, errors.New("range is not in format [A, B]")
	}

	to, from := rng[0], rng[1]
	if to > from {
		return 0, 0, errors.New("range must be in ascending order")
	}

	return to, from, nil
}

// From testing:
// - for 100 uuids, 36 empty buckets on avg.
// - for 200 uuids, 13 empty buckets on avg.
// - for 300 uuids, 4.9 empty buckets on avg.
// - for 400 uuids, 1.7 empty buckets on avg.
// - for 500 uuids, 0.6 empty buckets on avg.
// - for 600 uuids, 0.2 empty buckets on avg.
// - for 1000 uuids, the average is 0.005.
func getRolloutBucketWithSeed(v any, seed string) int {
	bucket := int64(murmur3.Sum32([]byte(fmt.Sprintf("%v", v))))
	offset := int64(murmur3.Sum32([]byte(seed)))
	return int((bucket + offset) % 100)
}

func watchFile(filePath string) error {
	previousStat, err := os.Stat(filePath)
	if err != nil {
		return err
	}

	for {
		time.Sleep(5 * time.Second)

		newStat, err := os.Stat(filePath)
		if err != nil {
			return err
		}

		if newStat.Size() != previousStat.Size() || newStat.ModTime() != previousStat.ModTime() {
			break
		}
	}

	return nil
}
