package scythe

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseTest(t *testing.T) {
	tests := []struct {
		text      string
		condition *flagTest
	}{
		{
			text: "key == something",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{"something"},
				Operator:  "==",
			},
		},
		{
			text: "key ==",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{""},
				Operator:  "==",
			},
		},
		{
			text: "key >= 5",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{"5"},
				Operator:  ">=",
			},
		},
		{
			text: "key > 5",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{"5"},
				Operator:  ">",
			},
		},
		{
			text: "key < 5",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{"5"},
				Operator:  "<",
			},
		},
		{
			text: "key <= 5",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{"5"},
				Operator:  "<=",
			},
		},
		{
			text: "uuid EXP [10, 50]",
			condition: &flagTest{
				Attribute: "uuid",
				Values:    []string{"10", "50"},
				Operator:  "EXP",
			},
		},
		{
			text: "key IN [a, b, c]",
			condition: &flagTest{
				Attribute: "key",
				Values:    []string{"a", "b", "c"},
				Operator:  "IN",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.text, func(t *testing.T) {
			condition, err := parseTest(test.text)
			require.NoError(t, err)
			assert.Equal(t, test.condition, condition)
		})
	}
}
